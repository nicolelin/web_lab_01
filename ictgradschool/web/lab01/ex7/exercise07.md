#`Herbivorous`
* Horse
* Cow
* Sheep
* Frog
* Capybara

#`Carnivorous`
* Cat
* Dog
* Polar Bear
* Hawk
* Lion

#`Omnivorous`
* Rat
* Fox
* Raccoon
* Chicken